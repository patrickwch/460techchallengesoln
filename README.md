# 460 Tech Challenge!!

### SUMMARY
This is the proposed solution to the 460 Tech Challenge.  

To achieve the deployment objective, we will run two sets of scripts.

 1. 460tcKeyGen.py
 2. 460tcCloudForm.yaml

Please following the Installation steps in the later part of this file.

In this solution, we utilise AWS cloudformation to create an EC2 instance, and apply relevant network configuration to ensure the EC2 instance have port 5002 and 22 open to the public.

## ASSUMPTION
Assuming the proposed solution is to be deployed in AWS.

## PREREQUISTS
Before you get started, you will need to have the following installed and configured:

 1. IAM user created in AWS, and obtain AWS access ID and Secret Key, (https://doc.aws.amaxon.com/cli/latest/userguide/cli-chap-getting-started.html)

 2. Boto3 library in Python, (https://aws.amazon.com/sdk-for-python)

 3. AWS Command Line Interface(CLI), (https://aws.amazon.com/cli)

Use "pip" and run the following command to install Boto3 and AWS CLI on your machine

    pip install awscli boto3

### Configure AWS Credential Locally
Once you have all the software prerequisites setup, and have obtained your IAM user details, you can now configure your environment to execute commands in AWS. 

Run the following command from your terminal

    aws configure

You will be prompted to provide your IAM user Access Key ID, Secret Key, Default AWS region and output format. Please use the details you have obtain in ***Prerequisite 1*** above. 

To check whether your AWS credential is setup with AWS CLI, run the following command. 

    aws ec2 describe-instances
This should return details of any EC2 instances running on AWS in the region in JSON format. Otherwise, an error is thrown and that means the credential didn't work
## INSTALLATION STEPS

We will now go through the following steps to deploy the project.
 1. Create Key Pair for EC2 & CloudFormation deployment
 2. Deploy a server into AWS with relevant configurations, and install the python application.
 3. Validate the deployment and installation 

### Step 1:   Create Key Pair for EC2 & CloudFormation Deployment

Run the python script ***"460tcKeyGen.py"*** in bash terminal

    python 460tcKeyGen.py

This program creates a key pair in AWS, as well as capturing and storing it on your local machine. The key pair file will be stored in your local folder, and change the key pair file to read only

### Step 2:  Deploy server into AWS with configuration 

Run the following AWS command in bash terminal, using file ***"460tcCloudForm.yaml"***

    aws cloudformation create-stack --stack-name Four60-Tech-Challenge-Stack --template-body file://460tcCloudForm.yaml
    

This AWS command will reference to 460tcCloudForm.yaml in the local folder, to create the EC2 instance on AWS with the relevant security configuration, as well as executing the bash commands & install the python application from Fabian's Github repository, with AWS CloudFormation.

If the script ran successfully, you should see a message returing the cloudFormation StackId as follow:

>  {"StackId": "arn:aws:cloudformation:us-west-2:290442375242:stack/Four60-Tech-Challenge-Stack/982f06a0-d842-11e8-86ab-06a411f6609a"}

### Step 3: Validation of Deployment

To validate whether we have successfully deploy the application, first we need to get the public IP of the EC2 instance that was stood up.

To get the elastic public ip of the EC2, type the following command

    aws cloudformation list-stack-resources --stack-name Four60-Tech-Challenge-Stack

This command will return the resources created under the cloudFormation command. You should find the public IP address of the EC2 under "***PhysicalResourceID***", and in this case, the public IP created for the EC2 is ***54.201.172.227***

> {
"ResourceStatus": "CREATE_IN_PROGRESS",
"ResourceType": "AWS::EC2::EIP",
"ResourceStatusReason": "Resource creation Initiated",
"LastUpdatedTimestamp": "2018-10-25T12:40:05.346Z",
"PhysicalResourceId": "54.201.172.227",
"LogicalResourceId": "460tcServerIpAddress"
}

Once we obtain the public IP of the EC2 that was created, simply type the following on your browser.

    http://54.201.172.227:5002

If the EC2 server is deployed and configured successfully, your browser will open up a webpage will the message "Hello World"


## FINDINGS & POSSIBLE IMPROVEMENTS
If time permits me to work further to improve the solution, here's the following that can be improved

#### Addressing Idempotency

I would install nginx as well.

##
The python script ***"server.py"*** has been amended (***Line 17***), in order to have the flask web app function accept connections from addresses other than localhost.

    #!/usr/bin/python
    
    from flask import Flask, request
    from flask_restful import Resource, Api
    
    app = Flask(__name__)
    api = Api(app)
    
    class Helloworld(Resource):
	    def get(self):
		    return 'Hello World'
    
    api.add_resource(Helloworld, '/')
    
    if __name__ == '__main__':
	    app.run(host = '0.0.0.0', port='5002')
The last line of the program, host ip has been adjusted to 0.0.0.0 instead of localhost 127.0.0.1.

> *This can be improved with more time. For example by installing nginx on the server. That will avoid us changing the server.py script.* 

### Address Anti-fragility
I would install and configured "supervisord", to monitor my functions, and also have "supervisord" to be configure to run at start up. This will ensure even when the server is crashed or reboot, out application will still be running.

## REMAKRS

This documentation should cover all the prerequisites and required steps to carry out the task.  
** comments**  are noted within the script file. It 

If there are any questions, feel free to contact Patrick Wong at patrickwch@gmail.com
 
 