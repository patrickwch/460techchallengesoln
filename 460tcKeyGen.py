import boto3
import os

ec2 = boto3.resource('ec2')


# create a file to store the key locally
outfile = open('460tckeygen-keypair.pem','w')

# call the boto ec2 function to create a key pair
key_pair = ec2.create_key_pair(KeyName='460tckeygen-keypair')

# capture the key and store it in a file
KeyPairOut = str(key_pair.key_material)
print(KeyPairOut)
outfile.write(KeyPairOut)

# key pair created, validation print message

print("Key Pair created to access EC2 instance, please check file")

# This will chmod 400 the key pair file to read only

os.chmod('460tckeygen-keypair.pem',0o400)

print("Key Pair now change to Read Only")